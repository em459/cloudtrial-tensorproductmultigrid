### COPYRIGHT AND LICENSE STATEMENT ###
#
#  This file is part of the TensorProductMultigrid code.
#  
#  (c) The copyright relating to this work is owned jointly by the
#  Crown, Met Office and NERC [2014]. However, it has been created
#  with the help of the GungHo Consortium, whose members are identified
#  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
#  
#  Main Developer: Eike Mueller
#  
#  TensorProductMultigrid is free software: you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#  
#  TensorProductMultigrid is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#  
#  You should have received a copy of the GNU Lesser General Public License
#  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
#  If not, see <http://www.gnu.org/licenses/>.
#
### COPYRIGHT AND LICENSE STATEMENT ###


###############################################################
###############################################################
#
#    Makefile for geometric multigrid project
#
# Eike Mueller, University of Bath, 2013
#
###############################################################
###############################################################

###############################################################
# Variables
###############################################################

# --- Compiler mode ---
MODE=Optimized

# --- Use MPI? ---
USEMPI=True

BLASLIB=-lblas
LAPACKLIB=-llapack

# Test convergence by comparing to analytical solution?
TESTCONVERGENCE=False

# Use piecewise linear interpolation in prolongation?
PIECEWISELINEAR=True

# Use piecewise linear interpolation in prolongation?
MEASURESMOOTHINGRATE=False

# Measure time spent in halo swaps only, do not solve
MEASUREHALOSWAP=False

# Use Cartesian geometry?
CARTESIANGEOMETRY=False

# Overlap communications and calculations?
OVERLAPCOMMS=True

# Use lapack?
USELAPACK=True

CPP=-cpp
HOSTNAME=$(shell hostname | sed 's/\([a-z]\+\)-.*/\1/')
HOSTNAME=cloud
# balena
ifeq ($(HOSTNAME),balena)
	CFLAGS=-xHOST -I${MKLROOT}/include/intel64/lp64 -I${MKLROOT}/include
  FC=mpiifort
	CPP=-fpp
	BLASLIB=${MKLROOT}/lib/intel64/libmkl_blas95_lp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_sequential.a -Wl,--end-group -lpthread -lm
	ifeq ($(USELAPACK),True)
		LAPACKLIB=${MKLROOT}/lib/intel64/libmkl_lapack95_lp64.a
	else
		LAPACKLIB=
	endif
endif
# aquila
ifeq ($(HOSTNAME),aquila)
  FC=mpif90
	CPP=-fpp
	BLASLIB=${MKLROOT}/lib/em64t/libmkl_blas95_lp64.a ${MKLROOT}/lib/em64t/libmkl_solver_lp64_sequential.a -Wl,--start-group ${MKLROOT}/lib/em64t/libmkl_intel_lp64.a ${MKLROOT}/lib/em64t/libmkl_core.a ${MKLROOT}/lib/em64t/libmkl_sequential.a -Wl,--end-group -lpthread -lm

	ifeq ($(USELAPACK),True)
		LAPACKLIB=${MKLROOT}/lib/em64t/libmkl_lapack95_lp64.a 
	else
		LAPACKLIB=
	endif
endif
# Hector
ifeq ($(HOSTNAME),hector)
  FC=ftn
	BLASLIB=
	LAPACKLIB=
endif
# Use version 4.4 of fortran compiler on Bath Uni machines

ifeq ($(HOSTNAME),mapc)
  ifeq ($(USEMPI),True)
	 	FC=mpif90
	else
		FC=gfortran44
	endif
endif

ifeq ($(HOSTNAME),eike-muellers-macbook.local)
  ifeq ($(USEMPI),True)
	 	FC=/usr/local/bin/mpif90
	else
		FC=gfortran
	endif
endif

ifeq ($(HOSTNAME),Eikes-MacBook-Pro.local)
  ifeq ($(USEMPI),True)
	 	FC=mpif90
	else
		FC=gfortran
	endif
endif

ifeq ($(HOSTNAME),cloud)
  ifeq ($(USEMPI),True)
    #FC=mpif90
    FC=mpiifort
  else
    FC=ifort
  endif
  #CFLAGS=-fdefault-integer-8 -m64 -I${MKLROOT}/include
  CFLAGS=-i8 -I${MKLROOT}/include -xHost	
  #BLASLIB= -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_gf_ilp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl
 BLASLIB=-Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_ilp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl
  LAPACKLIB=
endif


LIBFLAGS=$(BLASLIB) $(LAPACKLIB)

ifeq ($(MODE),Optimized)
	CFLAGS:=$(CFLAGS) -O3 $(CPP) -DNDEBUG
	LFLAGS=$(CFLAGS)
else
	CFLAGS:=$(CFLAGS) -O0 -g -Wall $(CPP) -ffpe-trap=invalid,overflow,zero \
		-fbacktrace
	LFLAGS=$(CFLAGS)
endif

ifeq ($(USEMPI),True)
	CFLAGS:=$(CFLAGS) -DUSE_MPI=USE_MPI
endif

ifeq ($(TESTCONVERGENCE),True)
	CFLAGS:=$(CFLAGS) -DTESTCONVERGENCE=TESTCONVERGENCE
endif

ifeq ($(PIECEWISELINEAR),True)
	CFLAGS:=$(CFLAGS) -DPIECEWISELINEAR=PIECEWISELINEAR
endif

ifeq ($(MEASURESMOOTHINGRATE),True)
	CFLAGS:=$(CFLAGS) -DMEASURESMOOTHINGRATE=MEASURESMOOTHINGRATE
endif

ifeq ($(MEASUREHALOSWAP),True)
	CFLAGS:=$(CFLAGS) -DMEASUREHALOSWAP=MEASUREHALOSWAP
endif

ifeq ($(CARTESIANGEOMETRY),True)
	CFLAGS:=$(CFLAGS) -DCARTESIANGEOMETRY=CARTESIANGEOMETRY
endif

ifeq ($(OVERLAPCOMMS),True)
	CFLAGS:=$(CFLAGS) -DOVERLAPCOMMS=OVERLAPCOMMS
endif

ifeq ($(USELAPACK),True)
	CFLAGS:=$(CFLAGS) -DUSELAPACK=USELAPACK
endif

# Main programs
MAIN= \
	mg_main.x

DEPENDENCIES=make.dependencies
SOURCES=$(wildcard *.f90)
OBJS:=$(patsubst %.f90,%.o,$(SOURCES))
OBJS:=$(filter-out $(patsubst %.x,%.o,$(MAIN)),$(OBJS))

###############################################################
# Targets
###############################################################

# Main target
.phony: all
all: $(DEPENDENCIES) $(MAIN)

# Include from makedepend output
include $(DEPENDENCIES)

# Generic rule for creating object file
%.o : %.f90
	$(FC) -c $(CFLAGS) $< -o $@

# Update dependencies
$(DEPENDENCIES): $(SOURCES)
	python makedepend.py $(DEPENDENCIES)

# Main programs
%.x: %.o $(OBJS)
	$(FC) $(LFLAGS) -o $@ $< $(OBJS) $(LIBFLAGS)


# Tidy up
.phony: clean
clean:
	rm -rf *~ $(MAIN) *.o $(DEPENDENCIES) *.mod
