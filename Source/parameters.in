! *********************************************************************
! *********************************************************************
! **                                                                 **
! **    Parameter file for geometric multigrid code                  **
! **                                                                 **
! *********************************************************************
! *********************************************************************
!
! *********************************************************************
! * General parameters
! *********************************************************************
&parameters_general
  savefields = .T.              ! Save fields to disk?
/

! *********************************************************************
! * General solver parameters
! *********************************************************************
&parameters_solver
  solvertype = 1,            ! Solver type:
                             !   1 : Richardson iteration
                             !   2 : Conjugate gradient
  resreduction = 1.0d-8,     ! Required relative residual reduction
  maxiter = 100               ! Maximal number of iterations
/

! *********************************************************************
! * Conjugate gradient parameters
! *********************************************************************
&parameters_conjugategradient
  verbose = 0,                  ! Verbosity level
  maxiter = 100,                ! Maximal number of iterations
  resreduction = 1.0e-5,        ! Target residual reduction
  n_prec = 1                    ! Number of smoother applications in
                                ! preconditioner (N.B.: Using 0 is
                                ! inefficient, as the identity is used
                                ! for preconditioning, instead of using
                                ! unpreconditioned CG.)
/

! *********************************************************************
! * Grid parameters
! *********************************************************************
&parameters_grid
  n = 64,      ! Number of horizontal grid cells
  nz = 1,     ! Number of vertical grid cells
  L = 1.0,      ! Size in horizontal direction
  H = 0.01,     ! Size in vertical direction
  vertbc = 2,   ! Boundary conditions at top and bottom of the
                ! atmosphere. 1 = DIRICHLET, 2 = NEUMANN
                ! Note that Neumann boundary conditions only work
                ! for coarsening in the horizontal only, as they are
                ! not yet implemented in the prolongation operator.
  graded = .F.  ! Is the grid graded in the vertical direction?
/

! *********************************************************************
! * Parallel communication parameters
! *********************************************************************
&parameters_communication
  halo_size = 1  ! Size of halos (has to be 1 or 2)
/

! *********************************************************************
! * Model parameters
! *********************************************************************
!
! parameters of the Helmholtz operator
!
! -omega2*(d^2/dy^2 + d^2/dy^2 + lambda2*d^2/dz^2) u + delta u = RHS
!
&parameters_model
  omega2 = 1.0,
  lambda2 = 1.0,  ! Vertical coupling
  delta = 0.0d0       ! Size of constant term
/

! *********************************************************************
! * Smoother parameters
! *********************************************************************
!
! parameters of the smoother
!
&parameters_smoother
  smoother = 3,                 ! Smoother method
                                !   3 = line SOR
                                !   4 = line SSOR
                                !   6 = line Jacobi
  ordering = 2,                 ! Ordering of grid points (for smoother)

                                !   1 = lexicographic
                                !   2 = red-black ordering
!  rho = 0.5d0                   ! Overrelaxation parameter
! rho = 0.6666666666666666d0    ! Overrelaxation parameter
   rho = 1.0d0                   ! Overrelaxation parameter
/

! *********************************************************************
! * Multigrid parameters
! *********************************************************************
&parameters_multigrid
  verbose = 2,                  ! Verbosity level
  n_lev = 7,                    ! Number of levels
  lev_split = 0,                ! First level where data is pulled together
  n_presmooth = 1,              ! Number of presmoothing steps
  n_postsmooth = 1,             ! Number of postsmoothing steps
  n_coarsegridsmooth = 1,       ! Number of smoothing steps on coarsest level
  prolongation = 2,             ! Prologation method
                                !   1 = constant interpolation
                                !   2 = (tri-) linear interpolation
  restriction = 1,              ! Restriction method
                                !   1 = cell average
  coarsegridsolver = 2          ! Solver on coarsest grid
                                !   1 = use smoother
                                !   2 = Conjugate gradient
/
