!=== COPYRIGHT AND LICENSE STATEMENT ===
!
!  This file is part of the TensorProductMultigrid code.
!  
!  (c) The copyright relating to this work is owned jointly by the
!  Crown, Met Office and NERC [2014]. However, it has been created
!  with the help of the GungHo Consortium, whose members are identified
!  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
!  
!  Main Developer: Eike Mueller
!  
!  TensorProductMultigrid is free software: you can redistribute it and/or
!  modify it under the terms of the GNU Lesser General Public License as
!  published by the Free Software Foundation, either version 3 of the
!  License, or (at your option) any later version.
!  
!  TensorProductMultigrid is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU Lesser General Public License for more details.
!  
!  You should have received a copy of the GNU Lesser General Public License
!  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
!  If not, see <http://www.gnu.org/licenses/>.
!
!=== COPYRIGHT AND LICENSE STATEMENT ===


!==================================================================
!
!  General parameters
!
!    Eike Mueller, University of Bath, Feb 2012
!
!==================================================================

module parameters

  implicit none

! floating point precision. Always use rl_kind in code
  integer, parameter :: single_precision=4        ! single precision
  integer, parameter :: double_precision=8        ! double precision
  integer, parameter :: rl=double_precision       ! global switch between
                                                  ! single/double precision
! NOTE: As we still use BLAS subroutines, these need to be
!       replaced as well when switching between double and
!       single precision!
  real(kind=rl), parameter :: Tolerance = 1.0e-15

! Output units
  integer, parameter :: STDOUT = 6
  integer, parameter :: STDERR = 0

! Numerical constants
  real(kind=rl), parameter :: two_pi = 6.2831853071795862_rl

end module parameters
