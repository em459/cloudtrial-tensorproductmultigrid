!=== COPYRIGHT AND LICENSE STATEMENT ===
!
!  This file is part of the TensorProductMultigrid code.
!  
!  (c) The copyright relating to this work is owned jointly by the
!  Crown, Met Office and NERC [2014]. However, it has been created
!  with the help of the GungHo Consortium, whose members are identified
!  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
!  
!  Main Developer: Eike Mueller
!  
!  TensorProductMultigrid is free software: you can redistribute it and/or
!  modify it under the terms of the GNU Lesser General Public License as
!  published by the Free Software Foundation, either version 3 of the
!  License, or (at your option) any later version.
!  
!  TensorProductMultigrid is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU Lesser General Public License for more details.
!  
!  You should have received a copy of the GNU Lesser General Public License
!  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
!  If not, see <http://www.gnu.org/licenses/>.
!
!=== COPYRIGHT AND LICENSE STATEMENT ===


!==================================================================
!
!  Geometric multigrid module for cell centred finite volume
!  discretisation.
!
!    Eike Mueller, University of Bath, Feb 2012
!
!==================================================================
module multigrid

  use mpi
  use parameters
  use datatypes
  use discretisation
  use messages
  use solver
  use conjugategradient
  use communication
  use timer

  implicit none

public::mg_parameters
public::mg_initialise
public::mg_finalise
public::mg_solve
public::measurehaloswap
public::REST_CELLAVERAGE
public::PROL_CONSTANT
public::PROL_TRILINEAR
public::COARSEGRIDSOLVER_SMOOTHER
public::COARSEGRIDSOLVER_CG

private

  ! --- multigrid parameter constants ---
  ! restriction
  integer, parameter :: REST_CELLAVERAGE = 1
  ! prolongation method
  integer, parameter :: PROL_CONSTANT = 1
  integer, parameter :: PROL_TRILINEAR = 2
  ! Coarse grid solver
  integer, parameter :: COARSEGRIDSOLVER_SMOOTHER = 1
  integer, parameter :: COARSEGRIDSOLVER_CG = 2

  ! --- Multigrid parameters type ---
  type mg_parameters
    ! Verbosity level
    integer :: verbose
    ! Number of MG levels
    integer :: n_lev
    ! First level where data is pulled together
    integer :: lev_split
    ! Number of presmoothing steps
    integer :: n_presmooth
    ! Number of postsmoothing steps
    integer :: n_postsmooth
    ! Number of smoothing steps on coarsest level
    integer :: n_coarsegridsmooth
    ! Prolongation (see PROL_... for allowed values)
    integer :: prolongation
    ! Restriction (see RESTR_... for allowed values)
    integer :: restriction
    ! Smoother (see SMOOTHER_... for allowed values)
    integer :: smoother
    ! Relaxation factor
    real(kind=rl) :: omega
    ! Smoother on coarse grid
    integer :: coarsegridsolver
    ! ordering of grid points for smoother
    integer :: ordering
  end type mg_parameters

! --- Parameters ---
  type(mg_parameters) :: mg_param
  type(model_parameters) :: model_param
  type(smoother_parameters) :: smoother_param
  type(grid_parameters) :: grid_param
  type(comm_parameters) :: comm_param
  type(cg_parameters) :: cg_param


! --- Gridded and scalar data structures ---
  ! Solution vector
  type(scalar3d), allocatable :: u(:,:)
  ! RHS vector
  type(scalar3d), allocatable :: b(:,:)
  ! residual
  type(scalar3d), allocatable :: r(:,:)

! --- Timer ---
  type(time), allocatable, dimension(:,:) :: t_restrict
  type(time), allocatable, dimension(:,:) :: t_prolongate
  type(time), allocatable, dimension(:,:) :: t_residual
  type(time), allocatable, dimension(:,:) :: t_addcorr
  type(time), allocatable, dimension(:,:) :: t_smooth
  type(time), allocatable, dimension(:,:) :: t_coarsesolve
  type(time), allocatable, dimension(:,:) :: t_total

contains

!==================================================================
! Initialise multigrid module, check and print out out parameters
!==================================================================
  subroutine mg_initialise(grid_param_in,     &  ! Grid parameters
                           comm_param_in,     &  ! Comm parameters
                           model_param_in,    &  ! Model parameters
                           smoother_param_in, &  ! Smoother parameters
                           mg_param_in,       &  ! Multigrid parameters
                           cg_param_in        &  ! CG parameters
                           )
    implicit none
    type(grid_parameters), intent(in)  :: grid_param_in
    type(comm_parameters), intent(in)  :: comm_param_in
    type(model_parameters), intent(in) :: model_param_in
    type(smoother_parameters), intent(in) :: smoother_param_in
    type(mg_parameters), intent(in)    :: mg_param_in
    type(cg_parameters), intent(in)    :: cg_param_in
    real(kind=rl)                      :: L, H
    integer                            :: n, nz, m, nlocal
    logical                            :: reduced_m
    integer                            :: level
    integer                            :: rank, ierr
    integer, dimension(2)              :: p_horiz
    integer, parameter                 :: dim_horiz = 2
    logical                            :: grid_active
    integer                            :: ix_min, ix_max, iy_min, iy_max
    integer                            :: icompx_min, icompx_max, &
                                          icompy_min, icompy_max
    integer                            :: halo_size
    integer                            :: vertbc
    character(len=32)                  :: t_label



    if (i_am_master_mpi) &
      write(STDOUT,*) '*** Initialising multigrid ***'
    ! Check that cell counts are valid
    grid_param = grid_param_in
    comm_param = comm_param_in
    mg_param = mg_param_in
    model_param = model_param_in
    smoother_param = smoother_param_in
    cg_param = cg_param_in
    halo_size = comm_param%halo_size
    vertbc = grid_param%vertbc

    ! Check parameters
    if (grid_param%n < 2**(mg_param%n_lev-1) ) then
      call fatalerror('Number of cells in x-/y- direction has to be at least 2^{n_lev-1}.')
    endif

    if (mod(grid_param%n,2**(mg_param%n_lev-1)) .ne. 0 ) then
      call fatalerror('Number of cells in x-/y- direction is not a multiple of 2^{n_lev-1}.')
    end if
    if (i_am_master_mpi) &
      write(STDOUT,*) ''

    ! Allocate memory for timers
    allocate(t_smooth(mg_param%n_lev,0:pproc))
    allocate(t_total(mg_param%n_lev,0:pproc))
    allocate(t_restrict(mg_param%n_lev,0:pproc))
    allocate(t_residual(mg_param%n_lev,0:pproc))
    allocate(t_prolongate(mg_param%n_lev,0:pproc))
    allocate(t_addcorr(mg_param%n_lev,0:pproc))
    allocate(t_coarsesolve(mg_param%n_lev,0:pproc))

    ! Allocate memory for all levels and initialise fields
    allocate(u(mg_param%n_lev,0:pproc))
    allocate(b(mg_param%n_lev,0:pproc))
    allocate(r(mg_param%n_lev,0:pproc))
    n = grid_param%n
    nlocal = n/(2**pproc)
    nz = grid_param%nz
    L = grid_param%L
    H = grid_param%H
    level = mg_param%n_lev
    m = pproc
    reduced_m = .false.
    ! Work out local processor coordinates (this is necessary to identify
    ! global coordinates)
    call mpi_comm_rank(MPI_COMM_HORIZ,rank,ierr)
    call mpi_cart_coords(MPI_COMM_HORIZ,rank,dim_horiz,p_horiz,ierr)
    if (i_am_master_mpi) then
      write(STDOUT, &
        '(" Global gridsize (x,y,z) (pproc = ",I4," )      : ",I8," x ",I8," x ",I8)') &
        pproc, n, n, nz
    end if
    do while (level > 0)
      if (i_am_master_mpi) &
        write(STDOUT, &
          '(" Local gridsize (x,y,z) on level ",I3," m = ",I4," : ",I8," x ",I8," x ",I8)') &
          level, m, nlocal, nlocal, nz
      if (nlocal < 1) then
        call fatalerror('Number of grid points < 1')
      end if

      ! Set sizes of computational grid (take care at boundaries)
      if (p_horiz(1) == 0) then
        icompy_min = 1
      else
        icompy_min = 1 - (halo_size - 1)
      end if

      if (p_horiz(2) == 0) then
        icompx_min = 1
      else
        icompx_min = 1 - (halo_size - 1)
      end if

      if (p_horiz(1) == 2**pproc-1) then
        icompy_max = nlocal
      else
        icompy_max = nlocal + (halo_size - 1)
      end if

      if (p_horiz(2) == 2**pproc-1) then
        icompx_max = nlocal
      else
        icompx_max = nlocal + (halo_size - 1)
      end if

      ! Allocate data
      allocate(u(level,m)%s(0:nz+1,                       &
                            1-halo_size:nlocal+halo_size, &
                            1-halo_size:nlocal+halo_size))
      allocate(b(level,m)%s(0:nz+1,                       &
                            1-halo_size:nlocal+halo_size, &
                            1-halo_size:nlocal+halo_size))
      allocate(r(level,m)%s(0:nz+1,                       &
                            1-halo_size:nlocal+halo_size, &
                            1-halo_size:nlocal+halo_size))
      u(level,m)%s(:,:,:) = 0.0_rl
      b(level,m)%s(:,:,:) = 0.0_rl
      r(level,m)%s(:,:,:) = 0.0_rl

      ! NB: 1st coordinate is in the y-direction of the processor grid,
      ! second coordinate is in the x-direction (see comments in
      ! communication module)
      iy_min = (p_horiz(1)/2**(pproc-m))*nlocal+1
      iy_max = (p_horiz(1)/2**(pproc-m)+1)*nlocal
      ix_min = p_horiz(2)/2**(pproc-m)*nlocal+1
      ix_max = (p_horiz(2)/2**(pproc-m)+1)*nlocal
      ! Set grid parameters and local data ranges
      ! Note that only n (and possibly nz) change as we
      ! move down the levels
      u(level,m)%grid_param%L = L
      u(level,m)%grid_param%H = H
      u(level,m)%grid_param%n = n
      u(level,m)%grid_param%nz = nz
      u(level,m)%grid_param%vertbc = vertbc
      u(level,m)%ix_min = ix_min
      u(level,m)%ix_max = ix_max
      u(level,m)%iy_min = iy_min
      u(level,m)%iy_max = iy_max
      u(level,m)%icompx_min = icompx_min
      u(level,m)%icompx_max = icompx_max
      u(level,m)%icompy_min = icompy_min
      u(level,m)%icompy_max = icompy_max
      u(level,m)%halo_size = halo_size

      b(level,m)%grid_param%L = L
      b(level,m)%grid_param%H = H
      b(level,m)%grid_param%n = n
      b(level,m)%grid_param%nz = nz
      b(level,m)%grid_param%vertbc = vertbc
      b(level,m)%ix_min = ix_min
      b(level,m)%ix_max = ix_max
      b(level,m)%iy_min = iy_min
      b(level,m)%iy_max = iy_max
      b(level,m)%icompx_min = icompx_min
      b(level,m)%icompx_max = icompx_max
      b(level,m)%icompy_min = icompy_min
      b(level,m)%icompy_max = icompy_max
      b(level,m)%halo_size = halo_size

      r(level,m)%grid_param%L = L
      r(level,m)%grid_param%H = H
      r(level,m)%grid_param%n = n
      r(level,m)%grid_param%nz = nz
      r(level,m)%grid_param%vertbc = vertbc
      r(level,m)%ix_min = ix_min
      r(level,m)%ix_max = ix_max
      r(level,m)%iy_min = iy_min
      r(level,m)%iy_max = iy_max
      r(level,m)%icompx_min = icompx_min
      r(level,m)%icompx_max = icompx_max
      r(level,m)%icompy_min = icompy_min
      r(level,m)%icompy_max = icompy_max
      r(level,m)%halo_size = halo_size

      ! Are these grids active?
      if ( (m == pproc) .or. &
           ( (mod(p_horiz(1),2**(pproc-m)) == 0) .and. &
             (mod(p_horiz(2),2**(pproc-m)) == 0) ) ) then
        grid_active = .true.
      else
        grid_active = .false.
      end if
      u(level,m)%isactive = grid_active
      b(level,m)%isactive = grid_active
      r(level,m)%isactive = grid_active
      write(t_label,'("t_total(",I3,",",I3,")")') level, m
      call initialise_timer(t_total(level,m),t_label)
      write(t_label,'("t_smooth(",I3,",",I3,")")') level, m
      call initialise_timer(t_smooth(level,m),t_label)
      write(t_label,'("t_restrict(",I3,",",I3,")")') level, m
      call initialise_timer(t_restrict(level,m),t_label)
      write(t_label,'("t_residual(",I3,",",I3,")")') level, m
      call initialise_timer(t_residual(level,m),t_label)
      write(t_label,'("t_prolongate(",I3,",",I3,")")') level, m
      call initialise_timer(t_prolongate(level,m),t_label)
      write(t_label,'("t_addcorrection(",I3,",",I3,")")') level, m
      call initialise_timer(t_addcorr(level,m),t_label)
      write(t_label,'("t_coarsegridsolver(",I3,",",I3,")")') level, m
      call initialise_timer(t_coarsesolve(level,m),t_label)

      ! If we are below L_split, split data
      if ( (level .le. mg_param%lev_split) .and. &
           (m > 0) .and. (.not. reduced_m) ) then
        reduced_m = .true.
        m = m-1
        nlocal = 2*nlocal
        cycle
      end if
      reduced_m = .false.
      level = level-1
      n = n/2
      nlocal = nlocal/2
    end do
    if (i_am_master_mpi) &
      write(STDOUT,*) ''
    call cg_initialise(cg_param)
  end subroutine mg_initialise

!==================================================================
! Finalise, free memory for all data structures
!==================================================================
  subroutine mg_finalise()
    implicit none
    integer :: level, m
    logical :: reduced_m
    character(len=80) :: s
    integer :: ierr

    if (i_am_master_mpi) &
      write(STDOUT,*) '*** Finalising multigrid ***'
    ! Deallocate memory
    level = mg_param%n_lev
    m = pproc
    reduced_m = .false.
    call print_timerinfo("--- V-cycle timing results ---")
    do while (level > 0)
      write(s,'("level = ",I3,", m = ",I3)') level,m
      call print_timerinfo(s)
      call print_elapsed(t_smooth(level,m),.True.,1.0_rl)
      call print_elapsed(t_restrict(level,m),.True.,1.0_rl)
      call print_elapsed(t_prolongate(level,m),.True.,1.0_rl)
      call print_elapsed(t_residual(level,m),.True.,1.0_rl)
      call print_elapsed(t_addcorr(level,m),.True.,1.0_rl)
      call print_elapsed(t_coarsesolve(level,m),.True.,1.0_rl)
      call print_elapsed(t_total(level,m),.True.,1.0_rl)
      deallocate(u(level,m)%s)
      deallocate(b(level,m)%s)
      deallocate(r(level,m)%s)
      ! If we are below L_split, split data
      if ( (level .le. mg_param%lev_split) .and. &
           (m > 0) .and. (.not. reduced_m) ) then
        reduced_m = .true.
        m = m-1
        cycle
      end if
      reduced_m = .false.
      level = level-1
    end do
    deallocate(u)
    deallocate(b)
    deallocate(r)
    deallocate(t_total)
    deallocate(t_smooth)
    deallocate(t_restrict)
    deallocate(t_prolongate)
    deallocate(t_residual)
    deallocate(t_addcorr)
    deallocate(t_coarsesolve)
      if (i_am_master_mpi) write(STDOUT,'("")')
  end subroutine mg_finalise

!==================================================================
! Restrict from fine -> coarse
!==================================================================
  subroutine restrict(phifine,phicoarse)
    implicit none
    type(scalar3d), intent(in)    :: phifine
    type(scalar3d), intent(inout) :: phicoarse
    integer :: ix,iy,iz
    integer :: ix_min, ix_max, iy_min, iy_max

    ix_min = phicoarse%icompx_min
    ix_max = phicoarse%icompx_max
    iy_min = phicoarse%icompy_min
    iy_max = phicoarse%icompy_max
    ! three dimensional cell average
    if (mg_param%restriction == REST_CELLAVERAGE) then
      ! Do not coarsen in z-direction
      do ix=ix_min,ix_max
        do iy=iy_min,iy_max
          do iz=1,phicoarse%grid_param%nz
            phicoarse%s(iz,iy,ix) =  &
              phifine%s(iz  ,2*iy  ,2*ix  ) + &
              phifine%s(iz  ,2*iy-1,2*ix  ) + &
              phifine%s(iz  ,2*iy  ,2*ix-1) + &
              phifine%s(iz  ,2*iy-1,2*ix-1)
          end do
        end do
      end do
    end if
  end subroutine restrict


!==================================================================
! Prolongate from coarse -> fine
! level, m is the correspong to the fine grid level
!==================================================================
  subroutine prolongate(level,m,phicoarse,phifine)
    implicit none
    integer, intent(in) :: level
    integer, intent(in) :: m
    type(scalar3d), intent(in) :: phicoarse
    type(scalar3d), intent(inout) :: phifine
    real(kind=rl) :: tmp
    integer :: nlocal
    integer, dimension(5) :: ixmin, ixmax, iymin, iymax
    integer :: n, nz
    integer :: ix, iy, iz
    integer :: dix, diy, diz
    real(kind=rl) :: rhox, rhoy, rhoz
    real(kind=rl) :: rho_i, sigma_j, h, c1, c2
    logical :: overlap_comms
    integer, dimension(4) :: send_requests, recv_requests
    integer :: ierr
    integer :: iblock

    ! Needed for interpolation matrix
#ifdef PIECEWISELINEAR
#else
    real(kind=rl) :: dx(4,3), A(3,3), dx_fine(4,2)
    integer :: i,j,k
    real(kind=rl) :: dxu(2), grad(2)
    dx(1,3) = 1.0_rl
    dx(2,3) = 1.0_rl
    dx(3,3) = 1.0_rl
    dx(4,3) = 1.0_rl
#endif

    nlocal = phicoarse%ix_max-phicoarse%ix_min+1
    n = phicoarse%grid_param%n
    nz = phicoarse%grid_param%nz

#ifdef OVERLAPCOMMS
    overlap_comms = (nlocal > 2)
#else
    overlap_comms = .false.
#endif
    ! Block 1 (N)
    ixmin(1) = 1
    ixmax(1) = nlocal
    iymin(1) = 1
    iymax(1) = 1
    ! Block 2 (S)
    ixmin(2) = 1
    ixmax(2) = nlocal
    iymin(2) = nlocal
    iymax(2) = nlocal
    ! Block 3 (W)
    ixmin(3) = 1
    ixmax(3) = 1
    iymin(3) = 2
    iymax(3) = nlocal-1
    ! Block 4 (E)
    ixmin(4) = nlocal
    ixmax(4) = nlocal
    iymin(4) = 2
    iymax(4) = nlocal-1
    ! Block 5 (INTERIOR)
    if (overlap_comms) then
      ixmin(5) = 2
      ixmax(5) = nlocal-1
      iymin(5) = 2
      iymax(5) = nlocal-1
    else
      ! If there are no interior cells, do not overlap
      ! communications and calculations, just loop over interior cells
      ixmin(5) = 1
      ixmax(5) = nlocal
      iymin(5) = 1
      iymax(5) = nlocal
    end if

    ! *** Constant prolongation or (tri-) linear prolongation ***
    if ( (mg_param%prolongation == PROL_CONSTANT) .or. &
         (mg_param%prolongation == PROL_TRILINEAR) ) then
      if (overlap_comms) then
        ! Loop over cells next to boundary (iblock = 1,...,4)
        do iblock = 1, 4
          if (mg_param%prolongation == PROL_CONSTANT) then
            call loop_over_grid_constant(iblock)
          end if
          if (mg_param%prolongation == PROL_TRILINEAR) then
            call loop_over_grid_linear(iblock)
          end if
        end do
        ! Initiate halo exchange
        call ihaloswap(level,m,phifine,send_requests,recv_requests)
      end if
      ! Loop over INTERIOR cells
      iblock = 5
      if (mg_param%prolongation == PROL_CONSTANT) then
        call loop_over_grid_constant(iblock)
      end if
      if (mg_param%prolongation == PROL_TRILINEAR) then
        call loop_over_grid_linear(iblock)
      end if
      if (overlap_comms) then
        if (m > 0) then
          call mpi_waitall(4,recv_requests, MPI_STATUSES_IGNORE, ierr)
        end if
      else
        call haloswap(level,m,phifine)
      end if
    else
      call fatalerror("Unsupported prolongation.")
    end if

    contains

    !------------------------------------------------------------------
    ! The actual loops over the grid for the individual blocks,
    ! when overlapping calculation and communication
    !------------------------------------------------------------------

    !------------------------------------------------------------------
    ! (1) Constant interpolation
    !------------------------------------------------------------------
    subroutine loop_over_grid_constant(iblock)
      implicit none
      integer, intent(in) :: iblock
      integer :: ix,iy,iz
      do ix=ixmin(iblock),ixmax(iblock)
        do iy=iymin(iblock),iymax(iblock)
          do dix = -1,0
            do diy = -1,0
              do iz=1,phicoarse%grid_param%nz
                phifine%s(iz,2*iy+diy,2*ix+dix) = phicoarse%s(iz,iy,ix)
              end do
            end do
          end do
        end do
      end do
    end subroutine loop_over_grid_constant

    !------------------------------------------------------------------
    ! (2) Linear interpolation
    !------------------------------------------------------------------
    subroutine loop_over_grid_linear(iblock)
      implicit none
      integer, intent(in) :: iblock
      integer :: ix,iy,iz
      do ix=ixmin(iblock),ixmax(iblock)
        do iy=iymin(iblock),iymax(iblock)
#ifdef PIECEWISELINEAR
          ! Piecewise linear interpolation
          do iz=1,phicoarse%grid_param%nz
            do dix = -1,0
              do diy = -1,0
                if ( (ix+(2*dix+1)+phicoarse%ix_min-1  < 1 ) .or. &
                     (ix+(2*dix+1)+phicoarse%ix_min-1  > n ) ) then
                  rhox = 0.5_rl
                else
                  rhox = 0.25_rl
                end if
                if ( (iy+(2*diy+1)+phicoarse%iy_min-1  < 1 ) .or. &
                     (iy+(2*diy+1)+phicoarse%iy_min-1  > n ) ) then
                  rhoy = 0.5_rl
                else
                  rhoy = 0.25_rl
                end if
                 phifine%s(iz,2*iy+diy,2*ix+dix) =      &
                  phicoarse%s(iz,iy,ix) +                &
                  rhox*(phicoarse%s(iz,iy,ix+(2*dix+1))  &
                        - phicoarse%s(iz,iy,ix)) +       &
                  rhoy*(phicoarse%s(iz,iy+(2*diy+1),ix)  &
                        - phicoarse%s(iz,iy,ix))
              end do
            end do
          end do
#else
          ! Fit a plane through the four neighbours of each
          ! coarse grid point. Use the gradient of this plane and
          ! the value of the field on the coarse grid point for
          ! the linear interpolation
          ! Calculate the displacement vectors
#ifdef CARTESIANGEOMETRY
          ! (ix-1, iy)
          dx(1,1) = -1.0_rl
          dx(1,2) =  0.0_rl
          ! (ix+1, iy)
          dx(2,1) = +1.0_rl
          dx(2,2) =  0.0_rl
          ! (ix, iy-1)
          dx(3,1) =  0.0_rl
          dx(3,2) = -1.0_rl
          ! (ix, iy+1)
          dx(4,1) =  0.0_rl
          dx(4,2) = +1.0_rl
#else
          rho_i = 2.0_rl*(ix+(phicoarse%ix_min-1)-0.5_rl)/n-1.0_rl
          sigma_j = 2.0_rl*(iy+(phicoarse%iy_min-1)-0.5_rl)/n-1.0_rl
          if (abs(rho_i**2+sigma_j**2) > 1.0E-12) then
            c1 = (1.0_rl+rho_i**2+sigma_j**2)/sqrt(rho_i**2+sigma_j**2)
            c2 = sqrt(1.0_rl+rho_i**2+sigma_j**2)/sqrt(rho_i**2+sigma_j**2)
          else
            rho_i = 1.0_rl
            sigma_j = 1.0_rl
            c1 = sqrt(0.5_rl)
            c2 = sqrt(0.5_rl)
          end if
          ! (ix-1, iy)
          dx(1,1) = -c1*rho_i
          dx(1,2) = +c2*sigma_j
          ! (ix+1, iy)
          dx(2,1) = +c1*rho_i
          dx(2,2) = -c2*sigma_j
          ! (ix, iy-1)
          dx(3,1) = -c1*sigma_j
          dx(3,2) = -c2*rho_i
          ! (ix, iy+1)
          dx(4,1) = +c1*sigma_j
          dx(4,2) = +c2*rho_i
          dx_fine(1,1) = 0.25_rl*(dx(1,1)+dx(3,1))
          dx_fine(1,2) = 0.25_rl*(dx(1,2)+dx(3,2))
          dx_fine(2,1) = 0.25_rl*(dx(2,1)+dx(3,1))
          dx_fine(2,2) = 0.25_rl*(dx(2,2)+dx(3,2))
          dx_fine(3,1) = 0.25_rl*(dx(1,1)+dx(4,1))
          dx_fine(3,2) = 0.25_rl*(dx(1,2)+dx(4,2))
          dx_fine(4,1) = 0.25_rl*(dx(2,1)+dx(4,1))
          dx_fine(4,2) = 0.25_rl*(dx(2,2)+dx(4,2))
#endif
          ! Boundaries
          if (ix-1+phicoarse%ix_min-1  < 1 ) then
            dx(1,1) = 0.5_rl*dx(1,1)
            dx(1,2) = 0.5_rl*dx(1,2)
          end if
          if (ix+1+phicoarse%ix_min-1  > n ) then
            dx(2,1) = 0.5_rl*dx(2,1)
            dx(2,2) = 0.5_rl*dx(2,2)
          end if
          if (iy-1+phicoarse%iy_min-1  < 1 ) then
            dx(3,1) = 0.5_rl*dx(3,1)
            dx(3,2) = 0.5_rl*dx(3,2)
          end if
          if (iy+1+phicoarse%iy_min-1  > n ) then
            dx(4,1) = 0.5_rl*dx(4,1)
            dx(4,2) = 0.5_rl*dx(4,2)
          end if
          ! Calculate matrix used for least squares linear fit
          A(:,:) = 0.0_rl
          do i = 1,4
            do j=1,3
              do k=1,3
                A(j,k) = A(j,k) + dx(i,j)*dx(i,k)
              end do
            end do
          end do
          ! invert A
          call invertA(A)
          do iz=1,phicoarse%grid_param%nz
            ! Calculate gradient on each level
            dxu(1:2) = dx(1,1:2)*phicoarse%s(iz,iy  ,ix-1) &
                     + dx(2,1:2)*phicoarse%s(iz,iy  ,ix+1) &
                     + dx(3,1:2)*phicoarse%s(iz,iy-1,ix  ) &
                     + dx(4,1:2)*phicoarse%s(iz,iy+1,ix  )
            grad(:) = 0.0_rl
            do j=1,2
              do k=1,2
                grad(j) = grad(j) + A(j,k)*dxu(k)
              end do
            end do
            ! Use the gradient and the field value in the centre of
            ! the coarse grid cell to interpolate to the fine grid
            ! cells
#ifdef CARTESIANGEOMETRY
            do dix=-1,0
              do diy=-1,0
                phifine%s(iz,2*iy+diy,2*ix+dix) =       &
                  phicoarse%s(iz,iy,ix)                 &
                  + 0.25_rl*( grad(1)*(2.0*dix+1)    &
                            + grad(2)*(2.0*diy+1))
              end do
            end do
#else
            phifine%s(iz,2*iy-1, 2*ix-1) = phicoarse%s(iz,iy,ix) + &
                                         ( grad(1)*dx_fine(1,1)  + &
                                           grad(2)*dx_fine(1,2) )
            phifine%s(iz,2*iy-1, 2*ix  ) = phicoarse%s(iz,iy,ix) + &
                                         ( grad(1)*dx_fine(2,1)  + &
                                           grad(2)*dx_fine(2,2) )
            phifine%s(iz,2*iy  , 2*ix-1) = phicoarse%s(iz,iy,ix) + &
                                         ( grad(1)*dx_fine(3,1)  + &
                                           grad(2)*dx_fine(3,2) )
            phifine%s(iz,2*iy  , 2*ix  ) = phicoarse%s(iz,iy,ix) + &
                                         ( grad(1)*dx_fine(4,1)  + &
                                           grad(2)*dx_fine(4,2) )
#endif
          end do
#endif
        end do
      end do
    end subroutine loop_over_grid_linear

  end subroutine prolongate

  !------------------------------------------------------------------
  ! Invert the 3x3 matrix A either using LaPack or the explicit
  ! formula
  !------------------------------------------------------------------
  subroutine invertA(A)
    implicit none
    real(kind=rl), intent(inout), dimension(3,3) :: A
    real(kind=rl), dimension(3,3) :: Anew
    real(kind=rl) :: invdetA
    integer :: ipiv(3), info
    real(kind=rl) :: work(3)
#ifdef USELAPACK
    call DGETRF( 3, 3, A, 3, ipiv, info )
    call DGETRI( 3, A, 3, ipiv, work, 3, info )
#else
    invdetA = 1.0_rl / ( A(1,1) * (A(3,3)*A(2,2) - A(3,2)*A(2,3)) &
                       - A(2,1) * (A(3,3)*A(1,2) - A(3,2)*A(1,3)) &
                       + A(3,1) * (A(2,3)*A(1,2) - A(2,2)*A(1,3)) )
    Anew(1,1) =     A(3,3)*A(2,2) - A(3,2)*A(2,3)
    Anew(1,2) = - ( A(3,3)*A(1,2) - A(3,2)*A(1,3) )
    Anew(1,3) =     A(2,3)*A(1,2) - A(2,2)*A(1,3)
    Anew(2,1) = - ( A(3,3)*A(2,1) - A(3,1)*A(2,3) )
    Anew(2,2) =     A(3,3)*A(1,1) - A(3,1)*A(1,3)
    Anew(2,3) = - ( A(2,3)*A(1,1) - A(2,1)*A(1,3) )
    Anew(3,1) =     A(3,2)*A(2,1) - A(3,1)*A(2,2)
    Anew(3,2) = - ( A(3,2)*A(1,1) - A(3,1)*A(1,2) )
    Anew(3,3) =     A(2,2)*A(1,1) - A(2,1)*A(1,2)
    A(:,:) = invdetA*Anew(:,:)
#endif
  end subroutine invertA

!==================================================================
! Multigrid V-cycle
!==================================================================
  recursive subroutine mg_vcycle(b,u,r,finelevel,splitlevel,level,m)
    implicit none
    integer, intent(in)                                     :: finelevel
    type(scalar3d), intent(inout), dimension(finelevel,0:pproc) :: b
    type(scalar3d), intent(inout), dimension(finelevel,0:pproc) :: u
    type(scalar3d), intent(inout), dimension(finelevel,0:pproc) :: r
    integer, intent(in)                                     :: splitlevel
    integer, intent(in)                                     :: level
    integer, intent(in)                                     :: m
    integer                                                 :: n_gridpoints
    integer                                                 :: nlocalx, nlocaly
    integer                                                 :: halo_size

    nlocalx = u(level,m)%ix_max-u(level,m)%ix_min+1
    nlocaly = u(level,m)%iy_max-u(level,m)%iy_min+1
    halo_size = u(level,m)%halo_size
    n_gridpoints = (nlocalx+2*halo_size) &
                 * (nlocaly+2*halo_size) &
                 * (u(level,m)%grid_param%nz+2)

    if (level > 1) then
      ! Perform n_presmooth smoothing steps
      call start_timer(t_smooth(level,m))
      call start_timer(t_total(level,m))
      call smooth(level,m,mg_param%n_presmooth, &
                  DIRECTION_FORWARD, &
                  b(level,m),u(level,m))
      call finish_timer(t_total(level,m))
      call finish_timer(t_smooth(level,m))
      ! Calculate residual
      call start_timer(t_residual(level,m))
      call start_timer(t_total(level,m))
      call calculate_residual(level,m,b(level,m),u(level,m),r(level,m))
      call finish_timer(t_total(level,m))
      call finish_timer(t_residual(level,m))
      ! Restrict residual
      call start_timer(t_restrict(level,m))
      call start_timer(t_total(level,m))
      call restrict(r(level,m),b(level-1,m))
      call finish_timer(t_total(level,m))
      call finish_timer(t_restrict(level,m))
      if ( ((level-1) .le. splitlevel) .and. (m > 0) ) then
        ! Collect data on coarser level
        call start_timer(t_total(level,m))
        call collect(level-1,m,b(level-1,m),b(level-1,m-1))
        call finish_timer(t_total(level,m))
        ! Set initial solution on coarser level to zero (incl. halos!)
        u(level-1,m-1)%s(:,:,:) = 0.0_rl
        ! solve on coarser grid
        call mg_vcycle(b,u,r,finelevel,splitlevel,level-1,m-1)
        ! Distribute data on coarser grid
        call start_timer(t_total(level,m))
        call distribute(level-1,m,u(level-1,m-1),u(level-1,m))
        call haloswap(level-1,m,u(level-1,m))
        call finish_timer(t_total(level,m))
      else
        ! Set initial solution on coarser level to zero (incl. halos!)
        u(level-1,m)%s(:,:,:) = 0.0_rl
        ! solve on coarser grid
        call mg_vcycle(b,u,r,finelevel,splitlevel,level-1,m)
      end if
      ! Prolongate error
      call start_timer(t_prolongate(level,m))
      call start_timer(t_total(level,m))
      call prolongate(level,m,u(level-1,m),r(level,m))
      call finish_timer(t_total(level,m))
      call finish_timer(t_prolongate(level,m))
      ! Add error to fine grid solution
      call start_timer(t_addcorr(level,m))
      call start_timer(t_total(level,m))
      call daxpy(n_gridpoints,1.0_rl,r(level,m)%s,1,u(level,m)%s,1)
      call finish_timer(t_total(level,m))
      call finish_timer(t_addcorr(level,m))
      ! Perform n_postsmooth smoothing steps
      call start_timer(t_smooth(level,m))
      call start_timer(t_total(level,m))
      call smooth(level,m, &
                  mg_param%n_postsmooth, &
                  DIRECTION_BACKWARD, &
                  b(level,m),u(level,m))
      call finish_timer(t_total(level,m))
      call finish_timer(t_smooth(level,m))
    else
      call start_timer(t_coarsesolve(level,m))
      call start_timer(t_total(level,m))
      if (mg_param%coarsegridsolver == COARSEGRIDSOLVER_CG) then
        call cg_solve(level,m,b(level,m),u(level,m))
      else if (mg_param%coarsegridsolver == COARSEGRIDSOLVER_SMOOTHER) then
        ! Smooth on coarsest level
        call smooth(level,m, &
                    mg_param%n_coarsegridsmooth,     &
                    DIRECTION_FORWARD, &
                    b(level,m),u(level,m))
      end if
      call finish_timer(t_total(level,m))
      call finish_timer(t_coarsesolve(level,m))
    end if

  end subroutine mg_vcycle

!==================================================================
! Test halo exchanges
!==================================================================
  recursive subroutine mg_vcyclehaloswaponly(b,u,r,finelevel,splitlevel,level,m)
    implicit none
    integer, intent(in)                                     :: finelevel
    type(scalar3d), intent(inout), dimension(finelevel,0:pproc) :: b
    type(scalar3d), intent(inout), dimension(finelevel,0:pproc) :: u
    type(scalar3d), intent(inout), dimension(finelevel,0:pproc) :: r
    integer, intent(in)                                     :: splitlevel
    integer, intent(in)                                     :: level
    integer, intent(in)                                     :: m
    integer, parameter :: nhaloswap = 100
    integer :: i
    integer :: ierr

    if (level > 1) then
      call mpi_barrier(MPI_COMM_HORIZ,ierr)
      do i=1,nhaloswap
        call haloswap(level,m,u(level,m))
      end do
      call mpi_barrier(MPI_COMM_HORIZ,ierr)
      if ( ((level-1) .le. splitlevel) .and. (m > 0) ) then
        call mpi_barrier(MPI_COMM_HORIZ,ierr)
        do i=1,nhaloswap
          call haloswap(level-1,m,u(level-1,m))
        end do
        call mpi_barrier(MPI_COMM_HORIZ,ierr)
        call mg_vcyclehaloswaponly(b,u,r,finelevel,splitlevel,level-1,m-1)
      else
        call mg_vcyclehaloswaponly(b,u,r,finelevel,splitlevel,level-1,m)
      end if
    else
      ! Haloswap on coarsest level
      call mpi_barrier(MPI_COMM_HORIZ,ierr)
      do i=1,nhaloswap
        call haloswap(level,m,u(level,m))
      end do
      call mpi_barrier(MPI_COMM_HORIZ,ierr)
    end if

  end subroutine mg_vcyclehaloswaponly

!==================================================================
! Multigrid solve
! Assumes that ghosts in initial solution are up-to-date
!==================================================================
  subroutine mg_solve(bRHS,usolution,solver_param)
    implicit none
    type(scalar3d), intent(in)      :: bRHS
    type(scalar3d), intent(inout)   :: usolution
    type(solver_parameters), intent(in) :: solver_param
    integer :: solvertype
    real(kind=rl)  :: resreduction
    integer :: maxiter
    integer :: n_gridpoints
    integer :: iter, level, finelevel, splitlevel
    real(kind=rl) :: res_old, res_new, res_initial
    logical :: solverconverged = .false.
    integer :: nlocalx, nlocaly
    integer :: halo_size
    type(time) :: t_prec, t_res, t_apply, t_l2norm, t_scalprod, t_mainloop
    type(scalar3d) :: pp
    type(scalar3d) :: q
    real(kind=rl) :: alpha, beta, pq, rz, rz_old
    integer :: ierr

    solvertype = solver_param%solvertype
    resreduction = solver_param%resreduction
    maxiter = solver_param%maxiter
    nlocalx = usolution%ix_max - usolution%ix_min+1
    nlocaly = usolution%iy_max - usolution%iy_min+1
    halo_size = usolution%halo_size

    level = mg_param%n_lev
    finelevel = level
    splitlevel = mg_param%lev_split

    ! Initialise timers
    call initialise_timer(t_prec,"t_prec")
    call initialise_timer(t_apply,"t_apply")
    call initialise_timer(t_l2norm,"t_l2norm")
    call initialise_timer(t_scalprod,"t_scalarprod")
    call initialise_timer(t_res,"t_residual")
    call initialise_timer(t_mainloop,"t_mainloop")

    ! Copy b to b(1)
    ! Copy usolution to u(1)
    n_gridpoints = (nlocalx+2*halo_size) &
                 * (nlocaly+2*halo_size) &
                 * (usolution%grid_param%nz+2)
    call dcopy(n_gridpoints,bRHS%s,1,b(level,pproc)%s,1)
    call dcopy(n_gridpoints,usolution%s,1,u(level,pproc)%s,1)
! Scale with volume of grid cells
    call volscale_scalar3d(b(level,pproc),1)
    call start_timer(t_res)
    call calculate_residual(level, pproc, &
                            b(level,pproc),u(level,pproc),r(level,pproc))
    call finish_timer(t_res)
    call start_timer(t_l2norm)
    res_initial = l2norm(r(level,pproc),.true.)
    call finish_timer(t_l2norm)
    res_old = res_initial
    if (mg_param%verbose > 0) then
      if (i_am_master_mpi) then
        write(STDOUT,'(" *** Multigrid solver ***")')
        write(STDOUT,'(" <MG> Initial residual : ",E10.5)') res_initial
      end if
    end if
    if (mg_param%verbose > 0) then
      if (i_am_master_mpi) then
        write(STDOUT,'(" <MG>   iter :   residual         rho")')
        write(STDOUT,'(" <MG> --------------------------------")')
      end if
    end if

    call mpi_barrier(MPI_COMM_WORLD,ierr)
    call start_timer(t_mainloop)
    if (solvertype == SOLVER_CG) then
      ! NB: b(level,pproc) will be used as r in the following
      call create_scalar3d(MPI_COMM_HORIZ,bRHS%grid_param,halo_size,pp)
      call create_scalar3d(MPI_COMM_HORIZ,bRHS%grid_param,halo_size,q)
      ! Apply preconditioner: Calculate p = M^{-1} r
      ! (1) copy b <- r
      call dcopy(n_gridpoints,r(level,pproc)%s,1,b(level,pproc)%s,1)
      ! (2) set u <- 0
      u(level,pproc)%s(:,:,:) = 0.0_rl
      ! (3) Call MG Vcycle
      call start_timer(t_prec)
      call mg_vcycle(b,u,r,finelevel,splitlevel,level,pproc)
      call finish_timer(t_prec)
      ! (4) copy pp <- u (=solution from MG Vcycle)
      call dcopy(n_gridpoints,u(level,pproc)%s,1,pp%s,1)
      ! Calculate rz_old = <pp,b>
      call start_timer(t_scalprod)
      call scalarprod(pproc,pp,b(level,pproc),rz_old)
      call finish_timer(t_scalprod)
      do iter = 1, maxiter
        ! Apply matrix q <- A.pp
        call start_timer(t_apply)
        call apply(pp,q)
        call finish_timer(t_apply)
        ! Calculate pq <- <pp,q>
        call start_timer(t_scalprod)
        call scalarprod(pproc,pp,q,pq)
        call finish_timer(t_scalprod)
        alpha = rz_old/pq
        ! x <- x + alpha*p
        call daxpy(n_gridpoints,alpha,pp%s,1,usolution%s,1)
        ! b <- b - alpha*q
        call daxpy(n_gridpoints,-alpha,q%s,1,b(level,pproc)%s,1)
        ! Calculate norm of residual and exit if it has been
        ! reduced sufficiently
        call start_timer(t_l2norm)
        res_new = l2norm(b(level,pproc),.true.)
        call finish_timer(t_l2norm)
        if (mg_param%verbose > 1) then
          if (i_am_master_mpi) then
            write(STDOUT,'(" <MG> ",I7," : ",E10.5,"  ",F10.5)') iter, res_new, res_new/res_old
          end if
        end if
        if (res_new/res_initial < resreduction) then
          solverconverged = .true.
          exit
        end if
        res_old = res_new
        ! Apply preconditioner q <- M^{-1} b
        ! (1) Initialise solution u <- 0
        u(level,pproc)%s(:,:,:) = 0.0_rl
        ! (2) Call MG Vcycle
        call start_timer(t_prec)
        call mg_vcycle(b,u,r,finelevel,splitlevel,level,pproc)
        call finish_timer(t_prec)
        ! (3) copy q <- u (solution from MG Vcycle)
        call dcopy(n_gridpoints,u(level,pproc)%s,1,q%s,1)
        call start_timer(t_scalprod)
        call scalarprod(pproc,q,b(level,pproc),rz)
        call finish_timer(t_scalprod)
        beta = rz/rz_old
        ! p <- beta*p
        call dscal(n_gridpoints,beta,pp%s,1)
        ! p <- p+q
        call daxpy(n_gridpoints,1.0_rl,q%s,1,pp%s,1)
        rz_old = rz
      end do
      call destroy_scalar3d(pp)
      call destroy_scalar3d(q)
    else if (solvertype == SOLVER_RICHARDSON) then
      ! Iterate until convergence
      do iter=1,maxiter
        call start_timer(t_prec)
        call mg_vcycle(b,u,r,finelevel,splitlevel,level,pproc)
        call finish_timer(t_prec)
        call start_timer(t_res)
        ! Ghosts are up-to-date here, so no need for halo exchange
        call calculate_residual(level, pproc, &
                                b(level,pproc),u(level,pproc),r(level,pproc))
        call finish_timer(t_res)
        call start_timer(t_l2norm)
        res_new = l2norm(r(level,pproc),.true.)
        call finish_timer(t_l2norm)
        if (mg_param%verbose > 1) then
          if (i_am_master_mpi) then
            write(STDOUT,'(" <MG> ",I7," : ",E10.5,"  ",F10.5)') iter, res_new, res_new/res_old
          end if
        end if
        if (res_new/res_initial < resreduction) then
          solverconverged = .true.
          exit
        end if
        res_old = res_new
      end do
      ! Copy u(1) to usolution
      call dcopy(n_gridpoints,u(level,pproc)%s,1,usolution%s,1)
    end if
    call finish_timer(t_mainloop)

    ! Print out solver information
    if (mg_param%verbose > 0) then
      if (solverconverged) then
        if (i_am_master_mpi) then
          write(STDOUT,'(" <MG> Final residual    ||r|| = ",E12.6)') res_new
          write(STDOUT,'(" <MG> Solver converged in ",I7," iterations rho_{avg} = ",F10.5)') &
          iter, (res_new/res_initial)**(1./(iter))
        end if
      else
        if (i_am_master_mpi) then
          write(STDOUT,'(" <MG> Solver failed to converge after ",I7," iterations rho_{avg} = ",F10.5)') &
          maxiter, (res_new/res_initial)**(1./(iter))
        end if
      end if
    end if
    call print_timerinfo("--- Main iteration timing results ---")
    call print_elapsed(t_apply,.True.,1.0_rl)
    call print_elapsed(t_res,.True.,1.0_rl)
    call print_elapsed(t_prec,.True.,1.0_rl)
    call print_elapsed(t_l2norm,.True.,1.0_rl)
    call print_elapsed(t_scalprod,.True.,1.0_rl)
    call print_elapsed(t_mainloop,.True.,1.0_rl)
    if (i_am_master_mpi) write(STDOUT,'("")')
  end subroutine mg_solve

!==================================================================
! Test haloswap on all levels
!==================================================================
  subroutine measurehaloswap()
    implicit none
    integer :: iter, level, finelevel, splitlevel

    level = mg_param%n_lev
    finelevel = level
    splitlevel = mg_param%lev_split
    call mg_vcyclehaloswaponly(b,u,r,finelevel,splitlevel,level,pproc)
  end subroutine measurehaloswap

end module multigrid

