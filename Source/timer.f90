!=== COPYRIGHT AND LICENSE STATEMENT ===
!
!  This file is part of the TensorProductMultigrid code.
!  
!  (c) The copyright relating to this work is owned jointly by the
!  Crown, Met Office and NERC [2014]. However, it has been created
!  with the help of the GungHo Consortium, whose members are identified
!  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
!  
!  Main Developer: Eike Mueller
!  
!  TensorProductMultigrid is free software: you can redistribute it and/or
!  modify it under the terms of the GNU Lesser General Public License as
!  published by the Free Software Foundation, either version 3 of the
!  License, or (at your option) any later version.
!  
!  TensorProductMultigrid is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU Lesser General Public License for more details.
!  
!  You should have received a copy of the GNU Lesser General Public License
!  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
!  If not, see <http://www.gnu.org/licenses/>.
!
!=== COPYRIGHT AND LICENSE STATEMENT ===


!==================================================================
!
!  Timer module
!
!    Eike Mueller, University of Bath, Feb 2012
!
!==================================================================

module timer

  use mpi
  use parameters

  implicit none

public::initialise_timing
public::finalise_timing
public::time
public::initialise_timer
public::start_timer
public::finish_timer
public::print_timerinfo
public::print_elapsed

private

! Timer type
  type time
    character(len=32) :: label
    real(kind=rl) :: start
    real(kind=rl) :: finish
    integer :: ncall
    real(kind=rl) :: elapsed
  end type time

  ! id of timer output file
  integer, parameter :: TIMEROUT = 9

  ! used my MPI
  integer :: rank, ierr

contains

!==================================================================
! Initialise timer module
!==================================================================
  subroutine initialise_timing(filename)
    implicit none
    character(len=*), intent(in) :: filename
    call mpi_comm_rank(MPI_COMM_WORLD,rank,ierr)
    if (rank==0) then
      open(UNIT=TIMEROUT,FILE=trim(filename))
      write(STDOUT,'("Writing timer information to file ",A40)') filename
      write(TIMEROUT,'("# ----------------------------------------------")')
      write(TIMEROUT,'("# Timer information for geometric multigrid code")')
      write(TIMEROUT,'("# ----------------------------------------------")')
    end if
  end subroutine initialise_timing

!==================================================================
! Finalise timer module
!==================================================================
  subroutine finalise_timing()
    implicit none
    if (rank==0) then
      close(TIMEROUT)
    end if
  end subroutine finalise_timing

!==================================================================
! Initialise timer
!==================================================================
  subroutine initialise_timer(t,label)
    implicit none
    type(time), intent(inout) :: t
    character(len=*), intent(in) :: label
    t%label = label
    t%start = 0.0_rl
    t%ncall = 0
    t%finish = 0.0_rl
    t%elapsed = 0.0_rl
  end subroutine initialise_timer

!==================================================================
! Start timer
!==================================================================
  subroutine start_timer(t)
    implicit none
    type(time), intent(inout) :: t
    t%start = mpi_wtime()
  end subroutine start_timer

!==================================================================
! Finish timer
!==================================================================
  subroutine finish_timer(t)
    implicit none
    type(time), intent(inout) :: t
    t%finish = mpi_wtime()
    t%elapsed = t%elapsed + (t%finish-t%start)
    t%ncall = t%ncall + 1
  end subroutine finish_timer

!==================================================================
! Print to timer file
!==================================================================
  subroutine print_timerinfo(msg)
    implicit none
    character(len=*), intent(in) :: msg
    if (rank == 0) then
      write(TIMEROUT,*) "# " // trim(msg)
    end if
  end subroutine print_timerinfo

!==================================================================
! Print timer information
!==================================================================
  subroutine print_elapsed(t,summaryonly,factor)
    implicit none
    type(time), intent(in)        :: t
    logical, intent(in)           :: summaryonly
    real(kind=rl), intent(in)     :: factor
    real(kind=rl) :: elapsedtime
    real(kind=rl) :: t_min
    real(kind=rl) :: t_max
    real(kind=rl) :: t_avg
    integer :: rank, nprocs, ierr
    integer :: nc

    elapsedtime = (t%elapsed) * factor
    call mpi_reduce(elapsedtime,t_min,1,MPI_DOUBLE_PRECISION, &
                    MPI_MIN, 0, MPI_COMM_WORLD,ierr)
    call mpi_reduce(elapsedtime,t_avg,1,MPI_DOUBLE_PRECISION, &
                    MPI_SUM, 0, MPI_COMM_WORLD,ierr)
    call mpi_reduce(elapsedtime,t_max,1,MPI_DOUBLE_PRECISION, &
                    MPI_MAX, 0, MPI_COMM_WORLD,ierr)
    call mpi_comm_size(MPI_COMM_WORLD,nprocs,ierr)
    call mpi_comm_rank(MPI_COMM_WORLD,rank,ierr)
    t_avg = t_avg/nprocs
    nc = t%ncall
    if (nc == 0) nc = 1
    if (summaryonly) then
      if (rank == 0) then
        write(TIMEROUT,'(A32," [",I7,"]: ",E10.4," / ",E10.4," / ",E10.4," (min/avg/max)")') &
          t%label,t%ncall,t_min,t_avg,t_max
        write(TIMEROUT,'(A32,"    t/call: ",E10.4," / ",E10.4," / ",E10.4," (min/avg/max)")') &
          t%label,t_min/nc,t_avg/nc,t_max/nc
      end if
    else
      write(TIMEROUT,'(A32," : ",I8," calls ",E10.4," (rank ",I8,")")') &
        t%label,elapsedtime, rank
    end if
    write(TIMEROUT,'("")')
  end subroutine print_elapsed

end module timer
