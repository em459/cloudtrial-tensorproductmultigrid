### COPYRIGHT AND LICENSE STATEMENT ###
#
#  This file is part of the TensorProductMultigrid code.
#  
#  (c) The copyright relating to this work is owned jointly by the
#  Crown, Met Office and NERC [2014]. However, it has been created
#  with the help of the GungHo Consortium, whose members are identified
#  at https://puma.nerc.ac.uk/trac/GungHo/wiki .
#  
#  Main Developer: Eike Mueller
#  
#  TensorProductMultigrid is free software: you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#  
#  TensorProductMultigrid is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#  
#  You should have received a copy of the GNU Lesser General Public License
#  along with TensorProductMultigrid (see files COPYING and COPYING.LESSER).
#  If not, see <http://www.gnu.org/licenses/>.
#
### COPYRIGHT AND LICENSE STATEMENT ###


import sys
import math
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt
from Scalar3d import *

##################################################################
##################################################################

def Main(FilenameStem,nproc,Level,outFilename,showhalo):
  nproclin = int(math.sqrt(nproc))
  phi_a = []
  vmax = -1.E9
  vmin = +1.E9
  for p in range(nproc):
    Filename = FilenameStem+'_'+('%010d' % nproc)+'_'+('%010d' % p)+'.dat'
    phi = Scalar3d(Filename)
    phi_a.append(phi)
    if (phi.C.max() > vmax):
      vmax = phi.C.max()
    if (phi.C.min() < vmin):
      vmin = phi.C.min()

  for p in range(nproc):
    phi = phi_a[p]
    C = phi.C[:,:,Level]
    plt.subplot(nproclin,nproclin,p+1)
    ax = plt.gca()
    hx = phi.L/phi.n
    hy = phi.L/phi.n
    if (showhalo):
      dx = hx*phi.halosize
      dy = hy*phi.halosize
    else:
      dx = 0.0
      dy = 0.0
    xmin = (phi.ix_min-1)*hx-dx
    xmax = (phi.ix_max)*hx+dx
    ymin = (phi.iy_min-1)*hy-dy
    ymax = (phi.iy_max)*hy+dy
    ax.set_xlim(xmin,xmax)
    ax.set_ylim(ymax,ymin)
    ax.set_aspect('equal')
    p = plt.pcolor(phi.X,phi.Y,C,vmax=vmax,vmin=vmin)
    plt.colorbar(p)
  plt.savefig(outFilename)

##################################################################
##################################################################
if (__name__ == '__main__'):
  if ( (len(sys.argv) > 6) or (len(sys.argv) < 5)) :
    print 'Usage: python '+sys.argv[0]+' <filenamestem> <nproc> <level> <oufile> [<showhalo>]'
    sys.exit(0)
  FilenameStem = sys.argv[1]
  nproc = int(sys.argv[2])
  Level = int(sys.argv[3])
  outFilename = sys.argv[4]
  showhalo = False
  if (len(sys.argv) > 5):
    showhalo = (sys.argv[5] == 'showhalo')
  Main(FilenameStem,nproc,Level,outFilename,showhalo)
