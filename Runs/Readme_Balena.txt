Use the following module files (you can source setup_balena.bash for this):

module purge
module load intel/compiler/64/16.3.210
module load intel/mkl/64/11.3.3
module load intel/mpi/64/5.1.3.210

Compilation
===========
(1) Go to the Source subdirectory

(2) type 'make', this should build the source and produce an executable called 'mg_main.x'

Running
=======
(1) Go to the directory Source (or copy the executable 'mg_main.x' somewhere else)

(2) to run the code on 16 cores with a parameter file called 'parameters.in', use

   mpirun -n 16 ./mg_main.x parameters.in x

(the 'x' is not a typo, but required since gcc and ifort count the number of command line arguments differently. Since the 'x' is ignored, it could really be anything...)

(3) Timing output will be produced in the time 'timing.txt'. The key result is the time spent in the solver, i.e. the following line:

t_solve      t/call: 0.8167E-01 / 0.1210E+00 / 0.1277E+00 (min/avg/max)

Parameter files
===============

The key quantity that should be modified in the nameslist in the input file:

n = XXX in &parameters_grid

Sensible values on p processors are:

p = 16:  n = 512
p = 64:  n = 1024
p = 256: n = 2048

The current directory contains the corresponding files parameters_p16.in, parameters_p64.in and parameters_p256.in. In this configuration runs should only take a few secons to complete.

Balena jobscript
================
Use the slurm script 'jobscript_balena.slm' to carry out runs. You only need to change the number of processors, this will automatically use the correct parameter file. All run data will be stored in a directory which is created by the jobscript. Before using the job script, obviously compile the executable, which the script assumes to be in ../Source.

Upon completion, output is written to a file of the form slurm-JOBID.out and the output is in the directory multigrid_JOBID, e.g.:

[em459@balena-01 Runs]$ ls slurm-*out
slurm-2761512.out
[em459@balena-01 Runs]$ ls multigrid_2761512/
fort.9  mg_main.x  output.txt  parameters_p16.in  slurm_script  timing.txt
